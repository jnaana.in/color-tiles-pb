module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true
  },
  globals: {
    m: true
  },
  extends: ["eslint-config-prettier"],
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 6
  },
  plugins: ["eslint-plugin-prettier"]
};
