export const MIN_NO_SQUARES = 2;
export const MAX_NO_SQUARES = 20;
export const MIN_SIDE_OF_SQUARE = 30;
export const MAX_SIDE_OF_SQUARE = 250;
export const ZERO = 0;
export const MAX_COLOR_VALUE = 255;
export const HEADER_OFFSET = 56;
export const DIMENSION_OFFSET = 20;
