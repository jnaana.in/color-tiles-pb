import test from "ava";
import colorMerger from "./index";

test("colors merged", t => {
  const color1 = {
    r: 10,
    g: 20,
    b: 30,
    a: 1
  };
  const color2 = {
    r: 100,
    g: 20,
    b: 35,
    a: 0.8
  };
  const color1Percentage = 0.8;
  const finalColor = {
    r: Math.round((1 - color1Percentage) * 100 + color1Percentage * 10),
    g: Math.round((1 - color1Percentage) * 20 + color1Percentage * 20),
    b: Math.round((1 - color1Percentage) * 35 + color1Percentage * 30),
    a: parseFloat(
      ((1 - color1Percentage) * 0.8 + color1Percentage * 1).toFixed(1)
    )
  };

  t.deepEqual(colorMerger(color1, color2, color1Percentage), finalColor);
  t.pass();
});
