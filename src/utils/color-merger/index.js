/**
 * Merge 2 Colors
 * @param {Object} color1 
 * @param {Object} color2 
 * @param {Float} color1Percentage 
 */
export default function colorMerger(color1, color2, color1Percentage) {
  const COLOR1_PERCENTAGE = color1Percentage || 0.5;
  const COLOR2_PERCENTAGE = 1 - COLOR1_PERCENTAGE;
  var rgba = {
    r: Math.round(color1.r * COLOR1_PERCENTAGE + COLOR2_PERCENTAGE * color2.r),
    g: Math.round(color1.g * COLOR1_PERCENTAGE + COLOR2_PERCENTAGE * color2.g),
    b: Math.round(color1.b * COLOR1_PERCENTAGE + COLOR2_PERCENTAGE * color2.b),
    a: parseFloat(
      (color1.a * COLOR1_PERCENTAGE + COLOR2_PERCENTAGE * color2.a).toFixed(1)
    )
  };
  return rgba;
}

/**
 * Calculate Brightness of a color
 * Implementation taken from WC3 website.
 * @param {Number} r 
 * @param {Number} g 
 * @param {Number} b 
 */
export function brightness(r, g, b) {
  return (r * 299 + g * 587 + b * 114) / 1000;
}
