/**
 * Generate Random number for given min/max
 * @param {Number} min 
 * @param {Number} max 
 */
export default function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
