global.window = Object.assign(
  require("mithril/test-utils/domMock.js")(),
  require("mithril/test-utils/pushStateMock")()
);
import test from "ava";
import SampleComponent from "./index.js";

test("arrays are equal", t => {
  var component = SampleComponent();
  t.deepEqual([1, 2], [1, 2]);
  t.pass();
});
