import m from "mithril";
import Square from "../../../entities/square";
import getRandomNumber from "../../../utils/random-number";
import {
  MIN_NO_SQUARES,
  MAX_NO_SQUARES,
  ZERO,
  MIN_SIDE_OF_SQUARE,
  MAX_SIDE_OF_SQUARE
} from "../../../config/constants";
import "./styles.scss";

/**
 * Control Component is responsible for Populate/Shake/DropNodes
 */
export default function ControlComponent() {
  return {
    view: vnode => {
      const isAnySquaresPresent = vnode.attrs.squares.length > 0;
      let btnShakeSquareClass = isAnySquaresPresent
        ? "btn btn-active"
        : "btn btn-disabled";
      return m("div", { class: "pb-control-section" }, [
        m(
          "button",
          { class: "btn primary-btn", onclick: vnode.attrs.onPopulateSquares },
          "Populate Nodes"
        ),
        m(
          "button",
          {
            class: btnShakeSquareClass,
            onclick: vnode.attrs.onShakeSquares,
            disabled: !isAnySquaresPresent
          },
          "Shake Nodes"
        ),
        m(
          "button",
          {
            class: "btn",
            onclick: vnode.attrs.onDropSquares,
            disabled: !isAnySquaresPresent
          },
          "Drop Nodes"
        )
      ]);
    }
  };
}
