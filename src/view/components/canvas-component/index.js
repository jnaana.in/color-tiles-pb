import m from "mithril";
import SquareNode from "../square-node";
import "./styles.scss";
import Square from "../../../entities/square/index";

/**
 * Canvas Component where all nodes are displayed
 */
export default function CanvasComponent() {
  return {
    view: function(vnode) {
      const squares = vnode.attrs.squares.map(square => {
        return m(SquareNode, {
          square,
          onSquareMoveFinalized: vnode.attrs.onSquareMoveFinalized,
          onMergeSquares: vnode.attrs.onMergeSquares,
          onSquareSplit: vnode.attrs.onSquareSplit
        });
      });
      return m(
        "div",
        {
          class: "pb-canvas-section"
        },
        [squares]
      );
    }
  };
}
