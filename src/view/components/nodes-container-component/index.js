import m from "mithril";
import Square from "../../../entities/square";
import getRandomNumber from "../../../utils/random-number";
import {
  MIN_NO_SQUARES,
  MAX_NO_SQUARES,
  ZERO,
  MIN_SIDE_OF_SQUARE,
  MAX_SIDE_OF_SQUARE,
  HEADER_OFFSET,
  DIMENSION_OFFSET,
  MAX_COLOR_VALUE
} from "../../../config/constants";

import ControlComponent from "../control-component";
import CanvasComponent from "../canvas-component";
import colorMerger from "../../../utils/color-merger";

/**
 * Control Component is responsible for Populate/Shake/DropNodes
 */
export default function NodesContainerComponent() {
  let dimensions = null;

  /**
   * Get dimensions of the container
   */
  const getDimensions = () => {
    return {
      xValue:
        document.querySelector(".pb-canvas-section").clientWidth -
        DIMENSION_OFFSET,
      yValue:
        document.querySelector(".pb-canvas-section").clientHeight -
        DIMENSION_OFFSET
    };
  };

  /**
   * Populate squares based on settings 
   */
  const populateSquares = () => {
    const noOfSquare = getRandomNumber(MIN_NO_SQUARES, MAX_NO_SQUARES);
    dimensions = getDimensions();
    const squares = [];
    for (let i = 0; i < noOfSquare; i++) {
      const randomX = getRandomNumber(ZERO, dimensions.xValue);
      const randomY = getRandomNumber(ZERO, dimensions.yValue) + HEADER_OFFSET;
      const randomSide = getRandomNumber(
        MIN_SIDE_OF_SQUARE,
        MAX_SIDE_OF_SQUARE
      );
      const color = {
        r: getRandomNumber(ZERO, MAX_COLOR_VALUE),
        g: getRandomNumber(ZERO, MAX_COLOR_VALUE),
        b: getRandomNumber(ZERO, MAX_COLOR_VALUE),
        a: 1 //for now set to 1 always
      };
      const square = new Square(randomSide, randomX, randomY, color);
      squares.push(square);
    }
    return squares;
  };

  /**
   * Populate Square Handler
   * @param {Vnode} vnode 
   */
  const onPopulateSquares = vnode => {
    vnode.state.allSquares = populateSquares();
    m.redraw();
  };

  /**
   * Shake Squares handlers
   * @param {vnode} vnode 
   */
  const onShakeSquares = vnode => {
    dimensions = getDimensions();
    vnode.state.allSquares = vnode.state.allSquares.map(square => {
      const randomX = getRandomNumber(ZERO, dimensions.xValue);
      const randomY = getRandomNumber(ZERO, dimensions.yValue) + HEADER_OFFSET;
      square.setX(randomX);
      square.setY(randomY);
      return square;
    });
    m.redraw();
  };

  /**
   * Drop all nodes handlers
   * @param {Vnode} vnode 
   */
  const onDropSquares = vnode => {
    dimensions = getDimensions();
    vnode.state.allSquares = vnode.state.allSquares.map(square => {
      const newY =
        dimensions.yValue + HEADER_OFFSET + DIMENSION_OFFSET - square.side;
      square.setY(newY);
      return square;
    });
    m.redraw();
  };

  /**
   * Mere 2 squares
   * @param {Vnode} vnode 
   */
  const onMergeSquares = vnode => {
    return (square1, square2) => {
      if (square1.id === square2.id) {
        return;
      }

      const totalArea = square1.area() + square2.area();
      const sideOfNewSquare = Math.round(Math.sqrt(totalArea));
      const percentageOfSquare1 = Math.round(square1.area() / square1.area());
      const newColor = colorMerger(
        square1.color,
        square2.color,
        percentageOfSquare1
      );
      var newSquare = new Square(
        sideOfNewSquare,
        square1.x,
        square1.y,
        newColor
      );
      newSquare.setChildren([square1, square2]);
      let filteredSquares = vnode.state.allSquares
        .filter(sq => sq.id !== square1.id)
        .filter(sq => sq.id !== square2.id);
      filteredSquares.push(newSquare);
      vnode.state.allSquares = [].concat(filteredSquares);
      m.redraw();
    };
  };

  /**
   * Move a square node
   * @param {Vnode} vnode 
   */
  const onSquareMoveFinalized = vnode => {
    return (square, x, y) => {
      for (var i = 0; i < vnode.state.allSquares.length; i++) {
        if (vnode.state.allSquares[i].id === square.id) {
          square.setX(x - square.side / 2 || square.x);
          square.setY(y - square.side / 2 || square.y);
          vnode.state.allSquares[i] = square;
          m.redraw();
          break;
        }
      }
    };
  };

  /**
   * Split a merged square
   * @param {Vnode} vnode 
   */
  const onSquareSplit = vnode => {
    return square => {
      if (square.children.length === 0) return;
      for (var i = 0; i < vnode.state.allSquares.length; i++) {
        if (vnode.state.allSquares[i].id === square.id) {
          vnode.state.allSquares.splice(i, 1);
          vnode.state.allSquares = vnode.state.allSquares.concat(
            square.children
          );
          m.redraw();
          break;
        }
      }
    };
  };

  return {
    allSquares: [],
    view: vnode => {
      return [
        m(ControlComponent, {
          onPopulateSquares: onPopulateSquares.bind(null, vnode),
          onShakeSquares: onShakeSquares.bind(null, vnode),
          onDropSquares: onDropSquares.bind(null, vnode),
          squares: vnode.state.allSquares
        }),
        m(CanvasComponent, {
          squares: vnode.state.allSquares,
          onMergeSquares: onMergeSquares.bind(null, vnode)(),
          onSquareMoveFinalized: onSquareMoveFinalized.bind(null, vnode)(),
          onSquareSplit: onSquareSplit.bind(null, vnode)()
        })
      ];
    }
  };
}
