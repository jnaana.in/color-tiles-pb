import m from "mithril";
import Square from "../../../entities/square";
import { brightness } from "../../../utils/color-merger";
import "./styles.scss";

/**
 * Stateless Square Node
 */
export default function SquareNode() {
  let dragEnterLeaveStabilizerCounter = 0;

  /**
   * Drag Start Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   * 
   */
  const onDragStart = (event, vnode) => {
    addClass(vnode.dom, ["dragstart"]);
    removeClass(vnode.dom, ["dragging"]);

    // Set datatransfer 
    const selectorNode = {
      square: vnode.attrs.square,
      targetId: vnode.attrs.square.id
    };
    event.dataTransfer.effectAllowed = "copyMove";
    event.dataTransfer.setData("Text", JSON.stringify(selectorNode));
    //Clone a node
    cloneNode(event.target);
    if (event.target.id) {
      if (event.dataTransfer.setDragImage) {
        let targetElement = document.getElementById("cloned-node");
        targetElement.style.visiblity = "visible";
        targetElement.style.cursor = "move";
        const offset = targetElement.offsetHeight / 2;
        event.dataTransfer.setDragImage(targetElement, offset, offset);
      }
    }
  };

  /**
   * Drag End Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   */
  const onDragEnd = (event, vnode) => {
    dragEnterLeaveStabilizerCounter = 0;
    if (vnode.dom.classList.contains("dragging") > -1) {
      vnode.attrs.onSquareMoveFinalized(
        vnode.attrs.square,
        event.clientX,
        event.clientY
      );
      removeClonedNode();// remove cloned node once drag ends
    }
    addClass(vnode.dom, ["dragend"]);
    removeClass(vnode.dom, ["dragging", "dragstart", "dragover", "dragenter"]);
    setTimeout(() => {
      removeClass(vnode.dom, ["dragend"]);
    }, 300);
  };

  /**
   * Drag Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   */
  const onDrag = (event, vnode) => {
    addClass(vnode.dom, ["dragging"]);
    removeClass(vnode.dom, ["dragstart"]);
  };

  /**
   * DragEnter Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   */
  const onDragEnter = (event, vnode) => {
    if (event.preventDefault) event.preventDefault();
    dragEnterLeaveStabilizerCounter = dragEnterLeaveStabilizerCounter + 1;
    addClass(vnode.dom, ["dragenter"]);
  };

  /**
   * DragOver Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   */
  const onDragOver = (event, vnode) => {
    if (event.preventDefault) event.preventDefault();
    addClass(vnode.dom, ["dragover"]);
  };

  /**
   * DragLeave Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   */
  const onDragLeave = (event, vnode) => {
    dragEnterLeaveStabilizerCounter = dragEnterLeaveStabilizerCounter - 1;
    removeClass(vnode.dom, ["dragover"]);
    if (dragEnterLeaveStabilizerCounter === 0) {
      removeClass(vnode.dom, ["dragenter"]);
    }
  };
/**
   * Drop Handler
   * @param {Event} event 
   * @param {Vnode} vnode 
   */
  const onDrop = (event, vnode) => {
    if (event.stopPropagation) event.stopPropagation();
    if (event.preventDefault) event.preventDefault();
    dragEnterLeaveStabilizerCounter = 0;
    removeClass(vnode.dom, ["dragover", "dragenter"]);
    // Get the square which is dropped
    const squareDD = JSON.parse(event.dataTransfer.getData("Text")).square;
    // Get the square on which its dropped
    // The below conversion of vnode.attrs.square is unneccessary
    // TODO: COuld be a bug witt app code or framework code.
    const squareOnWhichDropped = new Square(
      vnode.attrs.square.side,
      vnode.attrs.square.x,
      vnode.attrs.square.y,
      vnode.attrs.square.color
    );
    squareOnWhichDropped.setId(vnode.attrs.square.id);
    squareOnWhichDropped.setChildren(vnode.attrs.square.children);
    const squareDnD = new Square(
      squareDD.side,
      squareDD.x,
      squareDD.y,
      squareDD.color
    );
    squareDnD.setChildren(squareDD.children);
    squareDnD.setId(JSON.parse(event.dataTransfer.getData("Text")).targetId);
    vnode.attrs.onMergeSquares(squareOnWhichDropped, squareDnD);
    //finally remove cloned node
    removeClonedNode();
  };

  const addClass = (dom, classes) => {
    for (let i = 0; i < classes.length; i++) dom.classList.add(classes[i]);
  };

  const removeClass = (dom, classes) => {
    for (let i = 0; i < classes.length; i++) dom.classList.remove(classes[i]);
  };

  const cloneNode = square => {
    const clonedSquareNode = square.cloneNode(true);
    clonedSquareNode.id = "cloned-node";
    clonedSquareNode.style.position = "absolute";
    clonedSquareNode.style.zindex = "0";
    clonedSquareNode.style.top = "-9999px";
    clonedSquareNode.style.left = "-9999px";
    clonedSquareNode.classList.remove("dragstart");
    clonedSquareNode.classList.add("pb-square-node--highlight");
    document.body.appendChild(clonedSquareNode);
  };

  const removeClonedNode = () => {
    const clonedNode = document.querySelector("#cloned-node");
    if (clonedNode) clonedNode.remove();
  };

  return {
    view: vnode => {
      let self = this;
      const square = vnode.attrs.square;
      const area = square.side * square.side;
      const textColor =
        brightness(square.color.r, square.color.g, square.color.b) < 123
          ? "#FFFFFF"
          : "#333";
      return m(
        "div",
        {
          id: square.id,
          style: {
            left: `${square.x}px`,
            top: `${square.y}px`,
            height: `${square.side}px`,
            width: `${square.side}px`,
            background: `rgba(${square.color.r},${square.color.g},${
              square.color.b
            },${square.color.a})`,
            color: textColor
          },
          draggable: true,
          childrenCount: `${square.children.length}`,
          class: "pb-square-node",
          ondragstart: (event, extra) => {
            onDragStart(event, vnode);
          },
          ondrag: event => {
            onDrag(event, vnode);
          },
          ondragend: event => {
            onDragEnd(event, vnode);
          },
          ondragover: event => {
            onDragOver(event, vnode);
          },
          ondragleave: event => {
            onDragLeave(event, vnode);
          },
          ondragenter: event => {
            onDragEnter(event, vnode);
          },
          ondrop: event => {
            onDrop(event, vnode);
          },
          ondblclick: event => {
            vnode.attrs.onSquareSplit(vnode.attrs.square);
          }
        },
        [
          m("span", `${area} px2`),
          m(
            "span",
            {
              class:
                square.children.length > 0
                  ? "pb-square-node--haschildren"
                  : "pb-square-node--nochildren"
            },
            square.children.length > 0 ? "Split Squares" : ""
          )
        ]
      );
    }
  };
}
