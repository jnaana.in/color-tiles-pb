// Individual pages
import IndexPage from "./pages/landing-page";

const $root = document.body.querySelector("#root");

const Routes = {
  "/": {
    render: function() {
      return m(IndexPage);
    }
  }
};

const DefaultRoute = "/";

export { Routes, DefaultRoute };

document.addEventListener(
  "DOMContentLoaded",
  () => {
    document.body.addEventListener("dragover", ev => {
      window.reliableScreenX = ev.clientX;
      window.reliableScreenY = ev.clientY;
    });
  },
  false
);
