import m from "mithril";
import NodesContainerComponent from "../components/nodes-container-component";

import "./styles.scss";

export default function() {
  return {
    view() {
      return m("div", { class: "pb-main-section" }, [
        m(NodesContainerComponent)
      ]);
    }
  };
}
