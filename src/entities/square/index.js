import uuidv1 from "uuid/v1";

/**
 * Square to hold side/color/x/y  
 * children(if merged)
 */
export default class Square {
  constructor(side, x, y, color) {
    this.side = side;
    this.color = color;
    this.x = x;
    this.y = y;
    this.children = [];
    this.id = uuidv1();
  }

  setChildren(children) {
    this.children = [].concat(children);
  }

  area() {
    return this.side * this.side;
  }

  setX(x) {
    this.x = x;
  }

  setY(y) {
    this.y = y;
  }

  setId(id) {
    this.id = id;
  }
}
